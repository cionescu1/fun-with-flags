import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.List;
import javax.swing.ImageIcon;
import java.awt.Image;
import javax.swing.text.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.Font;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Color;
import java.io.*;
import javax.imageio.*;
import java.net.URL;

class Win extends JPanel
{
    private Image icon = null;
    public Win ()
    {
        this.setBackground(Color.CYAN.brighter());
        animate();
    }

    public void animate()
    {
        repaint();
    }

    public void paintComponent(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g;
        try
        {
            icon = new ImageIcon(this.getClass().getResource("win.jpg")).getImage();
        }
        catch(Exception e)
        {}
        if(icon != null)
        {
            g2d.drawImage(icon,0, 0, null);
        }
        g2d.setFont(new Font("Tahoma", Font.BOLD, 30));
        g2d.drawString("You scored: "+Integer.toString(Inter.correct), 40, 110);

    }
}
