import java.io.*;
import java.util.Scanner;
import java.util.*;

class Parse
{
    // the parsing string will contain the error messages appeared while initialising the parser
    // they will be printed in the text box from the interface
    private String[] q;
    public static String error = null;
    public static String[][] question;

    private Parse() throws Exception
    {
        List<String> questions = new ArrayList<String>();
        File myFile = new File("questions.txt");
        try
        {
            Scanner scanner = new Scanner(myFile);
            while(scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                questions.add(line);
            }
        }
        catch(Exception e)
        {
            if(error == null)
            {
                error = "File not found!";
            }
            else
            {
                error += "File not found!";
            }
        }
        q = new String[questions.size()];
        questions.toArray(q);
        if(error == null)
        {
            error = parseData();
        }
        else
        {
            error += parseData();
        }
        System.out.println(error);
    }

    private String parseData()
    {
        for(int i=0; i < q.length; ++i)
        {
            String[] elem = q[i].split(",");
            if(elem.length != 7)
            {
                return "Line " + Integer.toString(i+1)+ " has less elements than required";
            }
            if(! (elem[6].equals("none") || elem[6].equals("None"))  )
            {
                return "Line " +
                    Integer.toString(i+1)+ " has invalid name for no photo";
            }

            if(! ( elem[1].equals(elem[5]) || elem[2].equals(elem[5])
                || elem[3].equals(elem[5]) || elem[4].equals(elem[5]) ))
            {
                return "Line " +
                    Integer.toString(i+1)+ " has an answer that does not match choices";
            }

            if(! (elem[0].contains("?")) )
            {
                return "Line " +
                    Integer.toString(i+1)+ " does not contain a question mark";
            }
        }
        return null;
    }

    public static void main(String args[])
    {
        try
        {
            Parse parse = new Parse();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}
